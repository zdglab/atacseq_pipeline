# ATAC-seq pipeline

by migdal (mmigdal@iimcb.gov.pl)

## About

**atacseq_pipeline** is a data analysis pipeline for ATAC-seq data. It 
starts at the raw FASTQ files and it's final result are peaks denoting 
nucleosome free regions toghether with thier count quantification.

## Pipeline summary

1. Raw reads QC ([FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/))
2. Adapter trimming ([NGmerge](https://github.com/jsh58/NGmerge))
3. Trimmed reads QC ([FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/))
4. Reads mapping ([bowtie2](http://bowtie-bio.sourceforge.net/bowtie2/index.shtml))
5. Mapped reads QC ([samtools](http://www.htslib.org/))
6. Reads quality filtering ([samtools](http://www.htslib.org/))
7. Mitochondrial reads filtering ([samtools](http://www.htslib.org/))
8. PCR duplicates filtering ([picard](https://broadinstitute.github.io/picard/))
9. Reads shifting ([deeptools alignmentSieve](https://deeptools.readthedocs.io/en/develop/content/tools/alignmentSieve.html))
10. Peak calling ([macs2](https://github.com/macs3-project/MACS))
11. Peaks quantification ([bedtools](https://bedtools.readthedocs.io/en/latest/))
12. QC summary ([multiqc](https://multiqc.info/))

## Quick start

1. Download atacseq_pipeline repository. 
2. Run install.sh script that that will download 
   [nextflow](https://www.nextflow.io/) binary and example data files.
3. Edit `design.csv` file to point the pipeline to your input FASTQ files.
4. Run the pipeline by running: `make data`.

If you would like to tweek some of the pipeline settings, eg. change the
reference genome, have a look at `pipeline.config` file.

## Replicates handling

The pipeline allows peak calling using information from multiple 
replicates using 
[Fisher's](https://en.wikipedia.org/wiki/Fisher%27s_method) method. 
This method allows combining p-values from replicated experiment to 
produce combined p-value. First per bp p-values track in bedGraph format 
are created using Macs2, then combined and peaks are called directly on
combined p-values. Here this procedure is implemented using Macs2 
subcommands. Any number of replicates can be combined in this way. 
