#!/bin/bash

SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
mkdir ${SCRIPTPATH}/bin
cd ${SCRIPTPATH}/bin
curl -s https://get.nextflow.io | bash
cd ${SCRIPTPATH}
