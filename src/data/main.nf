#!/usr/bin/env nextflow

/** Pipeline for the ATAC-seq peak calling with replicates handling. What is does:
      + trimm adapters
      + maps reads to reference genome
      + filter mapped reads based on alignment quality, MT reads, PCR duplicates
      + calls peaks using macs2
      + combines replicates using fisher method as implemented in macs2
      + bunch of qc's
*/

// Pipeline parameters

// TODO add input check !!!
// TODO expose config with parameters to user like path to genome file

// env
bin = params.bin
conda_envs = projectDir + "/conda_env"
multiqc_conf = projectDir + "/multiqc_config.yaml"

// miscellaneous
genome_file = file("${params.genome}") // path to fasta
cmbreps = params.cmbreps // boolean

// peak calling
peakCalling_keep_dup = "all"
peakCalling_extsize = 150 //integer
peakCalling_minlen = 150 // minimum length of peak, better to set it as peakCalling_extsize value
peakCalling_maxgap = 75 // maximum gap between significant points in a peak, better to set it as tag size (read length?)

// Input channels
Channel
  .fromPath(params.input, checkIfExists: true)
  .splitCsv(header:true, sep:",")
  .map { row -> [ row.samid, row.group, row.replicate, [ file(row.fastq_1, checkIfExists: true), file(row.fastq_2, checkIfExists: true) ] ] }
  .into{ fastqc_raw_reads;
         trimm_addapters_raw_reads }

Channel
  .fromPath("${multiqc_conf}")
  .set { multiqc_config_ch }

// Pipeline

bowtie2_index = genome_file.baseName
ref_dir = genome_file.getParent()
bowtie2_index_files = file("${ref_dir}/${bowtie2_index}.*")

if (bowtie2_index_files.size() == 1) {
  process buildBWAIndex {
    conda "$conda_envs/build_bowtie2_index.yml"
    publishDir "$projectDir/data/external/ref/", mode: "move"

    input:
    file genome from genome_file
    val bowtie2_index from bowtie2_index
    val name from genome_file.baseName

    output:
    file "*.bt2"
    val "${ref_dir}/${bowtie2_index}" into map_reads_index

    script:
    """
    bowtie2-build ${genome} ${bowtie2_index}
    """
  }
} else {
  Channel
    .value( "${ref_dir}/${bowtie2_index}" )
    .set { map_reads_index }
}

process fastqcRawReads {
  tag "$samid"
  conda "$conda_envs/fastqc_raw_reads.yml"
  publishDir "${params.outdir}", mode: "rellink"

  input:
  tuple val(samid), val(group), val(replicate), path(reads) from fastqc_raw_reads

  output:
  file "fastqc/*_fastqc.{zip,html}" into fastqc_multiqc

  script:
  """
  mkdir fastqc/
  fastqc -q -o fastqc/ $reads
  """
}

process trimmAdaptersRawReads {
  tag "$samid"
  conda "$conda_envs/trimm_adapters_raw_reads.yml"
  cpus 10

  input:
  tuple val(samid), val(group), val(replicate), path(reads) from trimm_addapters_raw_reads

  output:
  tuple val(samid), val(group), val(replicate), file("*_trimmed*.fastq.gz") into map_reads
  file "cutadapt/*.cutadapt.json" into cutadapt_multiqc

  script:
  """
  mkdir cutadapt
  cutadapt \
    --json cutadapt/${samid}.cutadapt.json \
    -j ${task.cpus} \
    -a ${params.cutadapt_adapter} \
    -A ${params.cutadapt_adapter} \
    -o ${samid}_trimmed.1.fastq.gz \
    -p ${samid}_trimmed.2.fastq.gz \
    ${reads[0]} ${reads[1]}
  """
}

process mapReads {
  tag "$samid"
  conda "$conda_envs/map_reads.yml"
  publishDir "${params.outdir}", pattern: "*.bowtie2.log", mode: "rellink"
  cpus 16

  input:
  tuple val(samid), val(group), val(replicate), path(trimmed_reads) from map_reads
  val ref_index from map_reads_index

  output:
  tuple val(samid), val(group), val(replicate), file("bowtie2/${samid}.bam") into mark_duplicates
  file "bowtie2/${samid}.bowtie2.log" into bowtie2_multiqc

  script:
  """
  mkdir bowtie2/

  echo "bowtie2 -x ${ref_index} -1 ${trimmed_reads[0]} -2 ${trimmed_reads[1]} -p 16 --very-sensitive -X 2000 --non-deterministic" > bowtie2/${samid}.bowtie2.log

  bowtie2 \
    -x ${ref_index} \
    -1 ${trimmed_reads[0]} \
    -2 ${trimmed_reads[1]} \
    -p 16 \
    --very-sensitive \
    -X 2000 \
    --non-deterministic \
    2>> bowtie2/${samid}.bowtie2.log \
    | samtools sort -O bam -o bowtie2/${samid}.bam
  """
}

process markDuplicates {
  tag "$samid"
  conda "$conda_envs/filter_pcr_reads.yml"
  publishDir "${params.outdir}", mode: "rellink"

  input:
  tuple val(samid), val(group), val(replicate), path(bam) from mark_duplicates

  output:
  tuple val(samid), val(group), val(replicate), file("bowtie2/${samid}.bam"), file("bowtie2/${samid}.bai") into filter_mt_reads
  tuple val(samid), file("bowtie2/${samid}.bam") into samtools_stats_unfiltered
  tuple file("bowtie2/${samid}.bam"), file("bowtie2/${samid}.bai") into summarize_filtering
  tuple val(samid), file ("picard/${samid}.dups.txt") into estimate_library_complexity
  file ("picard/${samid}.dups.txt") into picard_multiqc

  script:
  """
  mkdir picard/
  mkdir bowtie2/
  picard MarkDuplicates \
    -I ${bam} \
    -O bowtie2/${samid}.bam \
    -M picard/${samid}.dups.txt \
    --CREATE_INDEX true \
    --OPTICAL_DUPLICATE_PIXEL_DISTANCE ${params.optical_duplicate_pixel_dist} \
    --TAGGING_POLICY All
  """
}

process samtoolsStatsUnfiltered {
  tag "$samid"
  conda "$conda_envs/samtools_stats.yml"
  publishDir "${params.outdir}", mode: "rellink"

  input:
  tuple val(samid), file(bam) from samtools_stats_unfiltered
  file genome from genome_file

  output:
  file "samtools/${bam}.bc" into samtools_stats_multiqc
  file "samtools/${bam}.bc" into summarize_filtering_raw_stats

  script:
  """
  mkdir samtools/
  samtools stats \
    -r ${genome} \
    ${bam} \
    > samtools/${bam}.bc
  """
}

process estimateLibraryComplexity {
  tag "$samid"
  conda "$conda_envs/estimate_library_complexity.yml"
  publishDir "${params.outdir}", mode: "rellink"

  input:
  tuple val(samid), file(duplicates) from estimate_library_complexity

  output:
  tuple file("preseq/${samid}.txt"), file("preseq/${samid}_preseq_real_counts.txt") into preseq_multiqc

  script:
  """
  mkdir preseq/
  tail -n +12 ${duplicates} | head -n -1 | awk -v OFS="\t" '{print \$1,\$3}' | sed -e 's:\\.0::' > ${duplicates}.hist.txt
  preseq lc_extrap -H -e 200000000.000000 -o preseq/${samid}.txt ${duplicates}.hist.txt
  echo "${samid} "`awk -v n=0 '{n+=\$1*\$2} END{print(n)}' ${duplicates}.hist.txt`" "`awk -v n=0 '{n+=\$2} END{print(n)}' ${duplicates}.hist.txt` > preseq/${samid}_preseq_real_counts.txt
  """
}

process filterMTReads {
  tag "$samid"
  conda "$conda_envs/filter_mt_reads.yml"
  cpus 4

  input:
  tuple val(samid), val(group), val(replicate), file(bam), file(bai) from filter_mt_reads

  output:
  tuple val(samid), val(group), val(replicate), file("${samid}.nonMT.bam"), file("${samid}.nonMT.bam.bai") into qual_filter_reads

  script:
  """
  samtools view -H ${bam} \
    | grep "SN:" \
    | sed -e 's/.*SN://' -e 's/\\tLN:.*//' \
    | grep -v MT \
    | xargs samtools view -@ 4 -b ${bam} \
    > ${samid}.nonMT.bam
  samtools index ${samid}.nonMT.bam
  """
}

process qualFilterReads {
  tag "$samid"
  conda "$conda_envs/qual_filter_reads.yml"
  publishDir "${params.outdir}", mode: "rellink"
  cpus 4

  input:
  tuple val(samid), val(group), val(replicate), file(bam), file(bai) from qual_filter_reads

  output:
  tuple val(samid), val(group), val(replicate), file("bowtie2/${samid}.filtered.bam"), file("bowtie2/${samid}.filtered.bam.bai") into size_select_shift_reads
  tuple file("bowtie2/${samid}.filtered.bam"), file("bowtie2/${samid}.filtered.bam.bai") into plot_insert_size
  tuple file("bowtie2/${samid}.filtered.bam"), file("bowtie2/${samid}.filtered.bam.bai") into plot_pca

  script:
  multimap_params = params.keep_multi_map ? '' : '-q 1'
  """
  mkdir bowtie2/
  samtools view -@ 4 -b -f 3 -F 1804 ${multimap_params} ${bam} \
    > bowtie2/${samid}.filtered.bam
  samtools index bowtie2/${samid}.filtered.bam
  """
}

process summarizeFiltering {
  conda "$conda_envs/samtools.yml"

  input:
  file "bowtie2/*" from summarize_filtering.collect()

  output:
  file("filtering_summary_mqc.tsv") into filtering_summary_multiqc

  script:
  multimap_params = params.keep_multi_map ? '' : '-q 1'
  """
  out="filtering_summary_mqc.tsv"
  echo "# parent_id: 'Reads filtering summary'" > \${out}
  echo "# parent_name: 'Reads filtering summary'" >> \${out}
  echo "# description: 'Shows number of reads filtered on MT and quality filtering steps'" >> \${out}
  echo "# plot_type: 'table'" >> \${out}
  echo "# section_name: 'Reads filtering'" >> \${out}
  echo "Sample_Name\tTotal_Reads\tMT_Reads\tUnmapped Reads\tDuplicated Reads\tUsable Reads\tUniquely Mapped Reads" >> \${out}
  for f in bowtie2/*.bam; do
     samid=`basename \${f%.bam}`
     nonMTchr=`samtools view -H \${f} | grep "SN:" | sed -e 's/.*SN://' -e 's/\\tLN:.*//' | grep -v MT`
     total=`samtools view -c \${f}`
     mt=`samtools view -c \${f} MT`
     unmapped=`samtools view -c -f 4 \${f}`
     duplicates=`samtools view -c -f 1024 -F 2828 \${f} \${nonMTchr}`
     usable=`samtools view -c -f 3 -F 3852 ${multimap_params} \${f} \${nonMTchr}`
     unique=`samtools view -c -f 3 -F 3852 -q 1 \${f} \${nonMTchr}`
     echo "\${samid}\t\${total}\t\${mt}\t\${unmapped}\t\${duplicates}\t\${usable}\t\${unique}" >> \${out}
  done
  """
}

process plotInsertSize {
  conda "$conda_envs/plot_insert_size.yml"
  publishDir "${params.outdir}", mode: "rellink"
  cpus 4

  input:
  file "bowtie2/*" from plot_insert_size.collect()

  output:
  file "insert_size/insert_size_mqc.html" into insertsize_multiqc

  script:
  """
  mkdir insert_size
  for f in bowtie2/*.bam; do
    samtools stats \${f} > \${f%.bam}.bc
  done
  plot-insertsize bowtie2/*.bc > insert_size/insert_size_mqc.html
  """
}

process plotPCA {
 conda "$conda_envs/plot_pca.yml"
 publishDir "${params.outdir}", mode: "rellink"
 cpus 4

 input:
 file "bowtie2/*" from plot_pca.collect()

 output:
 file "pca/all_samples_pca.tab" into pca_multiqc

 script:
 """
 mkdir pca
 multiBamSummary bins \
   --bamfiles bowtie2/*.bam \
   --outFileName multibamsummary.npz \
   --smartLabels \
   --binSize 10000 \
   --distanceBetweenBins 0 \
   --numberOfProcessors 4 \
   --minMappingQuality 1

  plotPCA \
    -in multibamsummary.npz \
    --outFileNameData pca/all_samples_pca.tab \
    --log2
 """
}

process sizeSelectShiftReads {
  tag "$samid"
  conda "$conda_envs/size_select_shift_reads.yml"
  publishDir "${params.outdir}", mode: "rellink"
  cpus 4

  input:
  tuple val(samid), val(group), val(replicate), file(bam), file(bai) from size_select_shift_reads

  output:
  tuple val(samid), val(group), val(replicate), file("bowtie2/${out}"), file("bowtie2/${out}.bai") into peak_calling

  script:
  if ("${params.max_fragment_length}" > 0) {
    size_select = true
    out = bam.getBaseName() + ".shifted.frag.bam"
  } else {
    size_select = false
    out = bam.getBaseName() + ".shifted.bam"
  }
  """
  mkdir bowtie2/

  if ${size_select}; then
    alignmentSieve -b ${bam} -o ${out} --ATACshift --maxFragmentLength ${params.max_fragment_length} -p 4
  else
    alignmentSieve -b ${bam} -o ${out} --ATACshift -p 4
  fi

  samtools sort -o bowtie2/${out} -O BAM -@ 4  ${out}
  samtools index bowtie2/${out}
  """
}

// peak calling parameters
// 3a. Peak calling - MACS2
// https://docs.google.com/document/d/1f0Cm4vRyDQDu0bMehHD7P7KOMxTOP-HiNoIvL1VcBt8/edit
// TODO the final output is missing p-value, and q-value columns, n-1 and -2 columns in narrowPeak
process peakCalling {
  tag "$samid"
  conda "$conda_envs/peak_calling.yml"
  cpus 8
  publishDir "${params.outdir}", pattern: "macs2/*", mode: "rellink"

  input:
  set val(samid), val(group), val(replicate), file(bam), file(bai) from peak_calling

  output:
  file "macs2/${samid}_peaks.narrowPeak"
  file "macs2/${samid}_qvalue.bdg"
  tuple val(group), file("${samid}_pvalue.bdg") into peak_calling_elements

  script:
  coverage_track_extsize = peakCalling_extsize / 2
  llocal_bias_track_p = peakCalling_extsize / 10000
  """
  mkdir macs2/

  # bam to bed
  # macs2 has problems with reading BAM files all the time;
  # it's safer to convert to bed first...
  bedtools bamtobed -i ${bam} > bed

  # filter duplicates
  macs2 filterdup \
    -i bed \
    -f BED \
    --gsize ${params.genome_size} \
    -s 1 \
    --keep-dup ${peakCalling_keep_dup} \
    -o filterdup.bed

  # estimate genomic background
  # genomebck = number of contorl reads * fragment length / genome size
  genomebck=\$(wc -l filterdup.bed | cut -d " " -f 1)
  genomebck=\$(python -c "print(\${genomebck} * 150 / ${params.genome_size})")

  # build coverage track
  macs2 pileup \
    -i filterdup.bed \
    -f BED \
    -B \
    --extsize ${coverage_track_extsize} \
    -o coverage_track.bdg
  # add pseudocount to coverage track
  macs2 bdgopt \
    -i coverage_track.bdg \
    -m add \
    -p \${genomebck} \
    -o ${samid}_coverage_track.bdg

  # build llocal bias track
  macs2 pileup \
    -i filterdup.bed \
    -B \
    -f BED \
    --extsize 5000 \
    -o llocal.bdg
  macs2 bdgopt \
    -i llocal.bdg \
    -m multiply \
    -p ${llocal_bias_track_p} \
    -o llocal_norm.bdg

  # build maximum background noise
  # with keep-dup all 'tags after...' line is missing
  macs2 bdgopt \
    -i llocal_norm.bdg \
    -m max \
    -p \${genomebck} \
    -o local_lambda.bdg

  # calculate per bp p-values
  macs2 bdgcmp \
    -t ${samid}_coverage_track.bdg \
    -c local_lambda.bdg \
    -m ppois \
    -o ${samid}_pvalue.bdg

  # p2q
  macs2 bdgopt \
    -i ${samid}_pvalue.bdg \
    -m p2q \
    -o macs2/${samid}_qvalue.bdg
  # remove descriptor line
  tail -n +2 macs2/${samid}_qvalue.bdg > t && mv t macs2/${samid}_qvalue.bdg


  # call peaks
  macs2 bdgpeakcall \
    -i macs2/${samid}_qvalue.bdg \
    -c ${params.peak_calling_th_q} \
    -l ${peakCalling_minlen} \
    -g ${peakCalling_maxgap} \
    -o macs2/${samid}_peaks.narrowPeak
  # remove descriptor line
  tail -n +2 macs2/${samid}_peaks.narrowPeak > t && mv t macs2/${samid}_peaks.narrowPeak
  """
}

// fisher
if (cmbreps) {
  peak_calling_elements
    .groupTuple(by: [0])
    .set { fisher_peaks }

  // Combining replicates needs at least two replicates!
  process fisherPeaks {
    tag "$group"
    conda "$conda_envs/fisher_peaks.yml"
    publishDir "${params.outdir}", mode: "rellink"

    input:
    tuple val(group), file("inbdg") from fisher_peaks

    output:
    tuple val(group), file("macs2/${group}_cmb.qvalue.bdg") , file("macs2/${group}_cmb.narrowPeak")  into fisher_peaks_output

    script:
    """
    mkdir macs2/

    # combine p-values
    macs2 cmbreps \
      -i inbdg* \
      -m fisher \
      -o combined.pvalue.bdg

    # p2q
    macs2 bdgopt \
      -i combined.pvalue.bdg \
      -m p2q \
      -o macs2/${group}_cmb.qvalue.bdg
    # remove descriptor line
    tail -n +2 macs2/${group}_cmb.qvalue.bdg > t && mv t macs2/${group}_cmb.qvalue.bdg

    # call peaks
    macs2 bdgpeakcall \
      -i macs2/${group}_cmb.qvalue.bdg \
      -c ${params.peak_calling_th_q} \
      -l ${peakCalling_minlen} \
      -g ${peakCalling_maxgap} \
      -o macs2/${group}_cmb.narrowPeak
    # remove descriptor line
    tail -n +2 macs2/${group}_cmb.narrowPeak > t && mv t macs2/${group}_cmb.narrowPeak
    """
  }
}

process multiqc {
  conda "$conda_envs/multiqc.yml"
  publishDir "${params.outdir}", mode: "rellink"

  input:
  file conf from multiqc_config_ch
  file "fastqc/*" from fastqc_multiqc.collect()
  file "cutadapt/*" from cutadapt_multiqc.collect()
  file "bowtie2/*" from bowtie2_multiqc.collect()
  file "picard/*" from picard_multiqc.collect()
  file "samtools/*" from samtools_stats_multiqc.collect()
  file "preseq/*" from preseq_multiqc.collect()
  file "insert_size/insert_size_mqc.html" from insertsize_multiqc
  file "pca/all_samples_pca.tab" from pca_multiqc
  file "filtering_summary_mqc.tsv" from filtering_summary_multiqc

  output:
  file "multiqc_report.html"
  file "multiqc_data"

  script:
  """
  for f in preseq/*preseq_real_counts.txt; do
    cat \${f} >> preseq/preseq_real_counts.txt
    rm \${f}
  done
  multiqc .
  """
}

//// TODO REMOVE blacklist regions
